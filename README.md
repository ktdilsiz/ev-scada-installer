Note: Requires internet conncetion and only tested for ubuntu

Clone down the repo and then run the following in the directory:

sudo install_vscada.sh

This should install everything you need automatically

Some notes:
Be careful about changing directories or file names once install, there are
a lot of places to change them.

The easiest ways to change what actions are performed by the daemons is to edit
the *.sh files in the /home/scada directory

For optimal viewing use firefox to navigate to https://addons.mozilla.org/en-Us/firefox/addon/r-kiosk/ 
and download the addod it makes the screen much nicer

If you have any questions contact me at Domenick_Falco@yahoo.com