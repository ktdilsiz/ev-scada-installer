#! /bin/sh -
echo "Starting scada service..."
cd /home/scada/ev-scadad
bin/setup_vcan.sh
echo "started CAN network"
echo "installing requirements..."
pip3 install -r requirements.txt
echo "requirements are installed!"
echo "starting scadad..."
python3 -m scadad &

